package org.soap;

/**
 * @author liuxingtai
 * @create 2021-10-12-17:05
 * @site https://gitee.com/SoapLiu
 */
public interface Servlet {
    void init() throws Exception;

    void destory() throws Exception;

    void service(Request request,Response response) throws Exception;
}
