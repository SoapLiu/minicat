package org.soap;

import java.io.IOException;

/**
 * @author liuxingtai
 * @create 2021-10-12-17:12
 * @site https://gitee.com/SoapLiu
 */
public class SoapServlet extends HttpServlet {
    @Override
    public void doGet(Request request, Response response) {
        String content = "<h1>Hello SoapServlet ! Get</h1>";
        try {
            response.output(HttpProtocolUtil.getHttpHeader200(content.length()) + content);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void doPost(Request request, Response response) {
        String content = "<h1>Hello SoapServlet ! Post</h1>";
        try {
            response.output(HttpProtocolUtil.getHttpHeader200(content.length()) + content);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void init() throws Exception {

    }

    @Override
    public void destory() throws Exception {

    }
}
