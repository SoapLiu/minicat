package org.soap;

/**
 * @author liuxingtai
 * @create 2021-10-12-17:07
 * @site https://gitee.com/SoapLiu
 */
public abstract class HttpServlet implements Servlet{

    public abstract void doGet(Request request,Response response);

    public abstract void doPost(Request request,Response response);

    @Override
    public void service(Request request, Response response) throws Exception {
        if("GET".equalsIgnoreCase(request.getMethod())){
            doGet(request,response);
        }else {
            doPost(request,response);
        }
    }
}
